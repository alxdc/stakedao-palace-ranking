FROM python:3.8-slim AS compile-image

RUN mkdir /app
COPY /src /app

RUN apt-get update
RUN apt-get -y install gcc git
RUN git clone https://github.com/corpetty/py-etherscan-api
RUN cd py-etherscan-api && pip install --user .
RUN pip install --user -r /app/requirements.txt
RUN rm /app/requirements.txt

FROM python:3.8-slim
COPY --from=compile-image /root/.local /root/.local
COPY --from=compile-image /app /app

ENV PATH=/root/.local/bin:$PATH
WORKDIR /app
ENTRYPOINT ["python", "-u", "/app/main.py"]