The palace ranking script is used to fetch data from the Stake DAO "Palace" contract and to rank users according to the points they got by staking their xSDT tokens. As users are rewarded with claimable but limited NFTs, the scripts also keeps track of how many NFTs have been redeemed and how many are still available per tier. Once data has been processed, it is pushed to a publicly accessible Google Sheets document.

Live spreadsheet: https://docs.google.com/spreadsheets/d/1Ft3E2r840w750PDSJaqkjGx8bdhfBMau3o8oyDLbmJY

Docker image available at: https://hub.docker.com/r/alexandredacosta/stakedao-palace-ranking

To use the docker image, you have to mount your config.yaml and gskey.json files to /app/config as shown in the example docker-compose file.
