import sys
import argparse
import time
import yaml
import json
import os
import logging
from datetime import datetime
from operator import itemgetter
from operator import itemgetter

from etherscan.contracts import Contract
from etherscan.accounts import Account
from etherscan.transactions import Transactions
from web3 import Web3
import pygsheets

import nft

LOCAL_CONFIG_FILE = 'config/config.yaml'
LOCAL_KEYS_FILE = 'config/keys.yaml'
LOCAL_GSKEY_FILE = 'config/gskey.json'

def main():
    logger = logging.getLogger("MyApp")
    logger.setLevel(logging.DEBUG)

    # Console handler
    console_handle = logging.StreamHandler()
    console_handle.setLevel(logging.DEBUG)

    # Formatter
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    console_handle.setFormatter(formatter)

    logger.addHandler(console_handle)

    logger.info("Starting palace ranking application")

    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="configuration file path")
    parser.add_argument("--gskey", help="Google Sheets key file")
    args = parser.parse_args()

    # Config priorities: --config arg > local config.yaml
    if args.config:
        config_file = args.config
    elif os.path.exists(LOCAL_CONFIG_FILE):
        config_file = LOCAL_CONFIG_FILE
    else:
        logger.error("No config arg, nor local config file. Stopping application.")
        sys.exit(1)

    with open(config_file, "r") as config_yaml:
        config = yaml.full_load(config_yaml)

    if args.gskey:
        keys_file = args.keys
    elif os.path.exists(LOCAL_GSKEY_FILE):
        keys_file = LOCAL_KEYS_FILE
    else:
        logger.error("No keys arg, nor local keys file. Stopping application.")
        sys.exit(1)

    with open(keys_file, "r") as keys_yaml:
        keys_config = yaml.full_load(keys_yaml)

    if args.gskey:
        config['gsheet']['keyfile'] = args.gskey
    elif os.path.exists(LOCAL_GSKEY_FILE):
        config['gsheet']['keyfile'] = LOCAL_GSKEY_FILE
    else:
        logger.error("No gskey arg, nor local gskey file. Stopping application.")
        sys.exit(1)

    palace_ranking = PalaceRanking(config, keys_config, logger)
    palace_ranking.run()


class PalaceRanking():

    def __init__(self, config, keys_config, logger):
        self.config = config
        self.infura_key = keys_config['infura']['key']
        self.infura_url = config['infura']['url'] + self.infura_key
        self.web3 = Web3(Web3.HTTPProvider(self.infura_url))
        self.etherscan_key = keys_config['etherscan']['key']
        self.contracts_config = config['palace']['contracts']
        self.loop_delay = config['runtime']['loop_delay']
        self.logger = logger
        self.nft_types = config['nft']['types']
        self.nft_cache = nft.NFTCache(config['nft']['metadata_urls'], config['nft']['cache_file'])
        self.max_row = str(config['gsheet']['max_row'])

        self.init_contracts()
        self.base_contract = self.contracts[config['palace']['base_contract']]

        self.run()

    def init_contracts(self):
        self.contracts = {}
        for contract_config in self.contracts_config:
            address = contract_config['address']
            abi, contract_object = self.get_contract(address)
            self.contracts[address] = {}
            self.contracts[address]['address'] = address
            self.contracts[address]['type'] = contract_config['type']
            self.contracts[address]['abi'] = abi
            self.contracts[address]['contract_object'] = contract_object
            self.contracts[address]['account'] = self.get_account(address)

    def get_contract(self, contract_address):
        api = Contract(address=contract_address, api_key=self.etherscan_key)
        contract_abi = api.get_abi()
        contract = self.web3.eth.contract(address=contract_address, abi=contract_abi)

        return contract_abi, contract

    def get_account(self, contract_address):
        return Account(contract_address, api_key=self.etherscan_key)

    def run(self):

        gc = pygsheets.authorize(service_file=self.config['gsheet']['keyfile'])

        starttime = time.time()
        while True:
            self.logger.info("Fetching data")
            users = {}

            # # Look for redeem transactions and build users list
            self.logger.info(f'Looking for NFT transactions and building users list')

            for _, contract in self.contracts.items():
                account = contract['account']
                transactions_erc20 = account.get_transaction_page(page=1, offset=0, sort='des', erc20=True)
                transactions = account.get_transaction_page(page=1, offset=0, sort='des')

                if contract['type'] == 1:
                    method_offset = 2
                    id_offset = 70

                if contract['type'] == 2:
                    method_offset = 714
                    id_offset = 782

                for transaction in transactions:
                    user_address = transaction['from']
                    if transaction['input'][method_offset:method_offset+8] == 'db006a75' and transaction['txreceipt_status'] == '1':
                        nft_id = int((transaction['input'][id_offset:id_offset+4]).lstrip('0'), 16)
                        name = self.nft_cache.get_name(nft_id)
                        tier = self.nft_cache.get_tier(nft_id).lower()
                        self.logger.info(f'{user_address} has a {name} - {tier}, NFT id is {nft_id}')

                        # User already has a NFT
                        if user_address in users:
                            if name in users[user_address]:
                                if tier in users[user_address][name]:
                                    users[user_address][name][tier] = users[user_address][name][tier] + 1
                                else:
                                    users[user_address][name][tier] = 1
                            else:
                                users[user_address][name] = {}
                                users[user_address][name][tier] = 1
                        else:
                            users[user_address] = {}
                            users[user_address][name] = {}
                            users[user_address][name][tier] = 1

                # Go through all ERC20 transaction to find out who participated
                for transaction in transactions_erc20:
                    user_address = transaction['from']
                    if user_address not in users:
                        users[user_address] = {}

            count = 0
            points_list = []
            sum_common = 0
            sum_rare = 0
            sum_unique = 0

            redeemed = {}
            for name in self.nft_types:
                redeemed[name] = {}
                redeemed[name]['common'] = 0
                redeemed[name]['rare'] = 0
                redeemed[name]['unique'] = 0

            for key in users:
                points = '{:.3f}'.format(self.base_contract['contract_object'].functions.earned(self.web3.toChecksumAddress(key)).call() / 1e18)
                staked = '{:.1f}'.format(self.base_contract['contract_object'].functions.balanceOf(self.web3.toChecksumAddress(key)).call() / 1e18)

                nft_list = []
                nft_count = 0
                for name in self.nft_types:
                    if name == 'Pythia':
                        continue
                    if name in users[key]:
                        common = users[key][name]['common'] if 'common' in users[key][name] else 0
                        rare = users[key][name]['rare'] if 'rare' in users[key][name] else 0
                        unique = users[key][name]['unique'] if 'unique' in users[key][name] else 0

                        nft_count = nft_count + common + rare + unique

                        redeemed[name]['common'] += common
                        redeemed[name]['rare'] += rare
                        redeemed[name]['unique'] += unique

                        common = '' if common == 0 else common
                        rare = '' if rare == 0 else rare
                        unique = '' if unique == 0 else unique

                        nft_list.extend([common, rare, unique])
                    else:
                        nft_list.extend(['', '', ''])

                common = 0
                rare = 0
                unique = 0

                # sum_redeemed = common + rare + unique
                sum_redeemed = 0

                self.logger.info(f"User {key} has {points} points with {staked} xSDT and has redeemed {sum_redeemed} NFTs, {common} common, {rare} rare and {unique} unique")

                user_data = [key, points, staked, nft_count]
                user_data.extend(nft_list)
                points_list.append(user_data)

                count += 1

            self.logger.info(f"Number of users: {count}")
            self.logger.info("Pushing data to google sheet")
            sh = gc.open('Palace ranking')
            wks = sh.sheet1
            wks.clear('A3', 'M' + self.max_row)
            wks.update_values('A3', points_list)
            wks.sort_range('A3', 'M' + self.max_row, 1, sortorder='DESCENDING')
            wks.update_value('V1', datetime.now().astimezone().strftime("%d/%m/%Y - %H:%M:%S UTC%z"))

            index = 4
            for name in self.nft_types:
                if name == 'Pythia':
                    continue
                cell1a = 'V' + str(index)
                cell1b = 'W' + str(index)
                cell2a = 'V' + str(index+1)
                cell2b = 'W' + str(index+1)
                cell3a = 'V' + str(index+2)
                cell3b = 'W' + str(index+2)
                wks.update_value(cell1a, redeemed[name]['common'])
                wks.update_value(cell1b, self.config['nft']['total_common'] - redeemed[name]['common'])

                wks.update_value(cell2a, redeemed[name]['rare'])
                wks.update_value(cell2b, self.config['nft']['total_rare'] - redeemed[name]['rare'])

                if name == 'Leviathan':
                    wks.update_value(cell3a, 0)
                    wks.update_value(cell3b, 0)
                else:
                    wks.update_value(cell3a, redeemed[name]['unique'])
                    wks.update_value(cell3b, self.config['nft']['total_unique'] - redeemed[name]['unique'])

                index += 4

            self.logger.info(f"Waiting {self.loop_delay} minutes until fetching data again")
            time.sleep(self.loop_delay * 60)

if __name__ == "__main__":
    main()