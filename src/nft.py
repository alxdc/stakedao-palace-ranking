import requests
import json
import time
import os

# As requests to the ipfs gateway can be pretty slow,
# the NFTs metadata is stored into memory and in a cache file
class NFTCache():

    # Init and load cache file in memory
    def __init__(self, base_urls, cache_file):
        self.base_urls = base_urls
        self.cache_file = cache_file
        if os.path.exists(self.cache_file):
            with open(self.cache_file, 'r') as cache_file:
                cache_data = cache_file.read()

        if cache_data:
            self.cache = json.loads(cache_data)
        else:
            self.cache = {}

    # Read metadata from cache or requests it from ipfs if not in cache
    def get_metadata(self, nft_id):
        if str(nft_id) in self.cache:
            nft_metadata = self.cache[str(nft_id)]
        else:
            print(nft_id)
            nft_metadata = self._get_metadata_ipfs(nft_id)
            print(nft_metadata)
        return nft_metadata

    def get_tier(self, nft_id):
        for attribute in self.get_metadata(nft_id)['attributes']:
            if attribute['trait_type'] == 'Tier':
                return attribute['value']

    def get_name(self, nft_id):
        return self.get_metadata(nft_id)['name']

    # Request metadata from ipfs
    def _get_metadata_ipfs(self, nft_id):
        if nft_id > 444:
            base_url = self.base_urls[2]
        elif nft_id > 222:
            base_url = self.base_urls[1]
        else:
            base_url = self.base_urls[0]
        nft_metadata = json.loads(requests.get(base_url + str(nft_id)).content.decode())
        self._add_to_cache_file(nft_id, nft_metadata)
        return nft_metadata

    # Add metadata to cache and rewrite cache file
    def _add_to_cache_file(self, nft_id, nft_metadata):
        self.cache[nft_id] = nft_metadata
        with open(self.cache_file, 'w') as cache_file:
            cache_file.write(json.dumps(self.cache))